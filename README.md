# Directory Remover, a verry stupid replacement for "rm -rf" that uses "rm -rf".  
the above should tell it.
### Arguments:
  1. `--help`/`-h` : Shows help message.
  2. `--version`/`-v` : Shows version - license details.
  3. `--dir <directory>`/`-d <directory>` : Specifies directory to delete.

### License:
**Directory Remover** is licensed under [**GNU General Public License v3.0 (or later)** (Click to see)](https://www.gnu.org/licenses/gpl-3.0.en.html)
