#define DR_VERSION "alphav0.1"
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <string>
using namespace std;
string current_argument, launchedas;
stringstream buffer;

void version(){
  cout << "DirectoryRemover version: " << DR_VERSION << " (launched as: \"" << launchedas << "\"). Licensed under GNU GPL v3 (and/or later)." << endl <<
  "See \"https://gitlab.com/bosshyapma/cache-remover/\" for source code and" << endl <<
  "    \"https://www.gnu.org/licenses/gpl-3.0.en.html\" for license being used." << endl;
  exit(0);
}

void help(){
  cout << "DirectoryRemover (launched as: \"" << launchedas << "\") Help Text" << endl <<
  "--help / -h : Shows this help text." << endl << "--version / -v : Shows version and license info." << endl <<
  "--dir <directory> / -d <directory> : Specifies cache directory to delete." << endl <<
  "(don't expect me to put a lot of arguments to a program that only deletes a directory)" << endl;
  exit(0);
}

int main(int argc, char *argv[]){
  launchedas = argv[0];
  if (argv[1] == NULL) {
    cout << "No argument (--help/--version/--dir <directory>) specified to delete!" << endl << "See these three arguments by \"" << launchedas << "\"." << endl;
    exit(1);
  }
  for (int i = 1; i < argc; ++i){
    current_argument = argv[i];
    if (current_argument == "--help" || current_argument == "-h") help();
    else if (current_argument == "--version" || current_argument == "-v") version();
    else if (current_argument == "--dir" || current_argument == "-d") {
      i++; current_argument = argv[i];
      if (argv[i][0] == '-') {
	cout << "First letter of the given directory is a dash \"-\" (argument start), leaving..." << endl;
	exit(1);
      }
      else if (current_argument == "/") {
        cout << "Did you just told me to \"rm / -rf\"?!? That will wipe your entire disk! Not doing! Emergency leaving!!!..." << endl;
	exit(1);
      }
      else {
	system(("rm -rvf "+current_argument).c_str());
      }
    }
  }
}
